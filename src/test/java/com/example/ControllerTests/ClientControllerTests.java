package com.example.ControllerTests;

import com.example.mvc.controllers.ClientController;
import com.example.mvc.entities.Client;
import com.example.mvc.services.ClientService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ClientController.class)
public class ClientControllerTests {

    @MockBean
    ClientService clientService;

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void it_should_return_created_client() throws Exception {
        Client request = new Client();
        request.setNameSurname("test client");

        Client client = new Client();
        client.setNameSurname(request.getNameSurname());

        when(clientService.save(request)).thenReturn(client);
         mockMvc.perform(post("/clients")
                 .content(mapper.writeValueAsString(request))
                 .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(request.getNameSurname()));


    }
}
