package com.example.mvc.repositories.examples;


import com.example.mvc.entities.example.PermissionGroup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionGroupRepository extends CrudRepository<PermissionGroup, Long> {

    List<PermissionGroup> findByName(String name);
    List<PermissionGroup> findAll();

}
