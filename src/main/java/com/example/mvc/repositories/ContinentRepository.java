package com.example.mvc.repositories;

import com.example.mvc.entities.Continent;
import com.example.mvc.entities.ID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContinentRepository extends CrudRepository<Continent, Long> {
        List<Continent> findAll();
        Optional<Continent> findById(Long id);
        Continent save (Continent continent1);
        void delete (Continent var);
        void deleteById(Long id);
        void deleteById(ID continent);
    }

