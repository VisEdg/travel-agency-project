package com.example.mvc.repositories;

import com.example.mvc.entities.Country;
import com.example.mvc.entities.ID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CountryRepository extends CrudRepository<Country, Long> {
        List<Country> findAll();
        Optional<Country> findById(Long id);
        Country save (Country country1);
        void delete (Country var);
        void deleteById(Long id);
        void deleteById(ID country);
}
