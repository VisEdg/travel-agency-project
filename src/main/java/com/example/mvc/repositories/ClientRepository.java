package com.example.mvc.repositories;


import com.example.mvc.entities.Client;
import com.example.mvc.entities.ID;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;


public interface ClientRepository extends CrudRepository<Client, Long> {
    List<Client> findAll();

    Optional<Client> findById(Long id);

    Client save(Client client1);

    void delete(Client var);

    void deleteById(Long id);

    void deleteById(ID client);

}