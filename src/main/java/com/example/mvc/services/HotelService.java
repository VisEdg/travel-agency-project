package com.example.mvc.services;


import com.example.mvc.entities.Hotel;
import com.example.mvc.entities.ID;
import com.example.mvc.repositories.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.util.List;

@Service
@Transactional

public class HotelService {

    @Autowired
    private HotelRepository hotelRepository;

    public List<Hotel> findAllHotels() {
        return hotelRepository.findAll();
    }

    public Hotel save(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    public void delete(Hotel hotel) {
        hotelRepository.delete(hotel);
    }

    public void deleteById(@Valid ID hotel) {
        hotelRepository.deleteById(hotel.getId());
    }

    public Hotel findById(Long id) {
        return hotelRepository.findById(id).get();
    }
}
