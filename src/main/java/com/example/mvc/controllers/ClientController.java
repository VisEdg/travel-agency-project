package com.example.mvc.controllers;

import com.example.mvc.entities.Client;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/client")

public class ClientController {

    private final ClientService clientService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Client>> findAll() {
        List<Client> clients = clientService.findAllClients();
        return ResponseEntity.ok(clients);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<Client> findById(@PathVariable Long id) {
        Client clients = clientService.findById(id);
        return ResponseEntity.ok(clients);
    }

    @PostMapping("/save")
    public ResponseEntity<Client> save(@Valid @RequestBody Client client){
        Client client1 = clientService.save(client);
        return ResponseEntity.ok(client1);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Client client){
        clientService.delete(client);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID client) {
        clientService.deleteById(client);
        return ResponseEntity.ok((new Message("Record deleted")));

    }

}