package com.example.mvc.controllers;


import com.example.mvc.entities.City;
import com.example.mvc.entities.ID;
import com.example.mvc.entities.Message;
import com.example.mvc.services.CityService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/city")

public class CityController {

    private final CityService cityService;

    @GetMapping("/showAll")
    public ResponseEntity<List<City>> findAll() {
        List<City> cities = cityService.findAllCities();
        return ResponseEntity.ok(cities);
    }

    @GetMapping("/show/{id}")
    public ResponseEntity<City> findById(@PathVariable Long id) {
        City cities = cityService.findById(id);
        return ResponseEntity.ok(cities);
    }

    @PostMapping("/save")
    public ResponseEntity<City> save(@Valid @RequestBody City city){
        City city1 = cityService.save(city);
        return ResponseEntity.ok(city1);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody City city){
        cityService.delete(city);
        return ResponseEntity.ok((new Message("Record deleted")));
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID city) {
        cityService.deleteById(city);
        return ResponseEntity.ok((new Message("Record deleted")));

    }

}
