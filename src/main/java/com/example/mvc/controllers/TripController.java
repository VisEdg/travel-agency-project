package com.example.mvc.controllers;


import com.example.mvc.entities.*;
import com.example.mvc.services.TripService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/trip")
public class TripController {

    private final TripService tripService;

    @GetMapping("/showAll")
    public ResponseEntity<List<Trip>> findAll() {
        List<Trip> trips = tripService.findAllTrips();
        return ResponseEntity.ok(trips); //JSON
    }

    @PostMapping("/save")
    public ResponseEntity<Trip> save(@Valid @RequestBody Trip trip) {
        Trip t = tripService.save(trip);
        return ResponseEntity.ok(t);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<Message> delete(@Valid @RequestBody Trip trip) {
        tripService.delete(trip);
        return ResponseEntity.ok(new Message("Record deleted")); //JSON
    }

    @DeleteMapping("/deleteById")
    public ResponseEntity<Message> deleteById(@Valid @RequestBody ID trip) {
        tripService.deleteById(trip.getId());
        return ResponseEntity.ok(new Message("Record deleted")); //JSON
    }

    @GetMapping("/show/upcoming")
    public ResponseEntity<List<Trip>> showUpcomingTrips() {
        return ResponseEntity.ok(tripService.findTripsByDate(LocalDate.now())); //JSON
    }

    @GetMapping("/show/byLength/{length}")
    public ResponseEntity<List<Trip>> showByLength(@PathVariable Integer length) {
        return ResponseEntity.ok(tripService.findTripsByNumberOfDays(length)); //JSON
    }

    @PostMapping("/show/tripsByDateAndContinent")
    public ResponseEntity<List<Trip>> showByDateAndContinent(@RequestBody TripRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByDateAndContinent(request)); //JSON
    }

    @PostMapping("/show/tripsByCity")
    public ResponseEntity<List<Trip>> showByCity(@RequestBody TripRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByCity(request)); //JSON
    }

    @PostMapping("/show/tripsByHotel")
    public ResponseEntity<List<Trip>> showByHotel(@RequestBody TripRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByHotel(request)); //JSON
    }

    @PostMapping("/show/tripsByAirport")
    public ResponseEntity<List<Trip>> showByAirport(@RequestBody TripRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByAirport(request)); //JSON
    }

    @PostMapping("/show/tripsByDepartureInterval")
    public ResponseEntity<List<Trip>> showByDepartureInterval(@RequestBody DateRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByDepartureDateInterval(request)); //JSON
    }

    @PostMapping("/show/tripsByArrivalInterval")
    public ResponseEntity<List<Trip>> showByArrivalInterval(@RequestBody DateRequestObject request) {
        return ResponseEntity.ok(tripService.findTripsByArrivalDateInterval(request)); //JSON
    }


}
