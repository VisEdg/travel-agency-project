package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Country is mandatory")
    private String country;
    @NotBlank(message = "City is mandatory")
    private String city;
    @NotBlank(message = "Street name is mandatory")
    private String streetName;
    @NotBlank(message = "House number is mandatory")
    private String houseNumber;
    @NotBlank(message = "Apartment number is mandatory")
    private String apartmentNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;


}
