package com.example.mvc.entities;

import lombok.Data;

import java.util.Date;

@Data
public class DateRequestObject {
    private Date startDate;
    private Date endDate;
}
