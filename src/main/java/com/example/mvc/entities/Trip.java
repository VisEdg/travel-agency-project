package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Trip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @OneToOne
    private City locationFrom;
    @OneToOne
    private City locationTo;
    @OneToOne
    private Hotel hotel;
    @OneToOne
    private Airport airport;
    private Date departureDate;
    private Date returnDate;
    private Integer numberOfDays;
    private String type;
    private BigDecimal adultPrice;
    private BigDecimal childrenPrice;
    private boolean promoted;
    private Integer adultBeds;
    private Integer childBeds;

     @ManyToOne(fetch = FetchType.LAZY)
     private Purchase tripPurchase;

     @OneToMany
     private List<User> tripParticipants;

}
