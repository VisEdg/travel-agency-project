package com.example.mvc.entities;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Integer qualityRank;
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    private City city;

}
